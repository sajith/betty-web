Hacking Setup
------------------------------------------------------------------------

To get started, you will need the following things:

   - the Haskell tool "Stack"
   - GHC 8.0.2
   - PostgreSQL
   - libpq-dev
   - yesod-bin
   - GNU m4
   - GNU make

Please see the appropriate documentation for how to install each of
these.

In most cases Stack should handle setting up some pieces of the
development environment, such as GHC and cabal-install.  You should
install the rest.

GNU m4 and make were unfortunately necessary because I have weird
ideas about how to set up things; they should go away in the future.

The Yesod quickstart guide is particularly helpful:

    http://www.yesodweb.com/page/quickstart

To get started, we need (at least) two PostgreSQL databases: one for
development, and one for testing.  To quickly set up these databases:

    sudo -u postgres createuser betty
    sudo -u postgres createdb betty_devel
    sudo -u postgres createdb betty_test

    sudo -u postgres psql
    postgres=# alter user betty with encrypted password 'secret';
    ALTER ROLE
    postgres=# grant all privileges on database betty_devel to betty;
    GRANT
    postgres=# grant all privileges on database betty_test to betty;
    GRANT
    postgres=# \q

If you'd like an alternative for the "stock" psql client, consider
pgcli <https://www.pgcli.com/>.

We don't want to push passwords and other sensitive configuration
strings to a public repository by mistake.  These strings are saved in
a file named secrets.m4, and this secrets.m4 file is in .gitignore so
it isn't added to the git repository.  The top-level Makefile should
help to create the actual configuration files when you run 'make'.  It
is a little kludgey, but it works, for now anyway.

Create secrets.m4:

     cp secrets.m4.example secrets.m4

Edit secrets.m4 to use appropriate usernames and passwords, and then
run:

     make

This will create the right configuration, and you should be mostly
ready to build and run the web app:

    stack install yesod-bin
    stack build
    stack test

If everything went well, test cases run by 'stack test' should pass.
Now you can run:

    yesod devel

and start hacking!

(I suppose I should be sorry about using m4 this way.  If you have
better ideas, please let me know!)

