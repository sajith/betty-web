Considering contributing to this project?  How nice of you!

Send feature requests, bug reports, bug fixes, general criticisms, etc
in the way of the project maintainer: sajith@hcoop.net.

Also please refrain from sending abuses.

See docs/hacking.txt for information about development setup.

