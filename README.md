# About Betty

[![badge-gitlab](https://gitlab.com/sajith/betty-web/badges/master/build.svg)](https://gitlab.com/sajith/betty-web/pipelines)

This is the web side of a diabetes data management tool.  It uses
Yesod web framework, PosgreSQL, and other fun things.

Curious?  Please get in touch:

Sajith Sasidharan <sajith@hcoop.net>
